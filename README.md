# apq8096-tflite

ModalAi Tensorflow Lite with gpu delegate enabled

## Build Environment

This project builds in the voxl-cross docker image (>= V1.7)

Follow the instructions here to build and install the voxl-cross docker image:
https://gitlab.com/voxl-public/voxl-docker

## Build Instructions

1) pull in submodules

```bash
git submodule update --init --recursive
```

2) Launch the voxl-cross docker.

```bash
~/git/apq8096-tflite$ voxl-docker -i voxl-cross
voxl-cross:~$
```

3) build
inside docker:
```bash
./build.sh
./make_package.sh
```

4) Make an ipk while still inside the docker.

```bash
voxl-cross:~$ ./make_package.sh ipk
```

This will make a new package file in your working directory. The name and version number came from the package control file. If you are updating the package version, edit it there.

Optionally add the --timestamp argument to append the current data and time to the package version number in the package name. This is done automatically by the CI package builder for development and nightly builds, however you can use it yourself if you like.

## Deploy to VOXL

You can now push the ipk or deb package to VOXL and install with dpkg/opkg however you like. To do this over ADB, you may use the included helper script: deploy_to_voxl.sh. Do this outside of docker as your docker image probably doesn't have usb permissions for ADB.

The deploy_to_voxl.sh script will query VOXL over adb to see if it has dpkg installed. If it does then then .deb package will be pushed an installed. Otherwise the .ipk package will be installed with opkg.

```bash
(outside of docker)
apq8096-tflite$ ./deploy_to_voxl.sh
```

This deploy script can also push over a network given sshpass is installed and the VOXL uses the default root password.


```bash
(outside of docker)
apq8096-tflite$ ./deploy_to_voxl.sh ssh 192.168.1.123
```
