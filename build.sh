#!/bin/bash

# apply patches
echo "Applying MAI Patches"
patch -uN tensorflow/tensorflow/lite/CMakeLists.txt -i patches/CMakeLists.txt.patch
patch -uN tensorflow/tensorflow/lite/nnapi/nnapi_implementation_disabled.cc -i patches/nnapi_implementation_disabled.patch
patch -uN tensorflow/tensorflow/lite/tools/cmake/modules/ruy.cmake -i patches/ruy.cmake.patch

echo "Done Applying Patches"

mkdir -p build64
cd build64
cmake -DCMAKE_TOOLCHAIN_FILE=/opt/cross_toolchain/aarch64-gnu-4.9.toolchain.cmake -DBUILD_SHARED_LIBS=ON -DTFLITE_ENABLE_XNNPACK=OFF -DTFLITE_ENABLE_GPU=ON -DTFLITE_ENABLE_NNAPI=OFF -DCMAKE_CXX_FLAGS="${CMAKE_CXX_FLAGS} -std=c++14" ../tensorflow/tensorflow/lite

echo ""
echo "Building Tensorflow Lite"
cmake --build . -j
echo "Done building Tensorflow Lite"

echo ""
echo "Building Benchmark Model"
cmake --build . -j -t benchmark_model
echo "Done building Benchmark Model"

echo ""
echo "Build Complete!"

cd ..
